<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
Route::get('/', function () {
    return view('welcome');
});

 Route::get('time_log_in',function(){
     return view('time_log_in');
 })->name('time_log_in');

Route::post('time_log_in', timeController::class. '@button_function')->name('time_log_in');

Route::get ('time_log_out',function(){
    return view('time_log_out');
})->name('time_log_out');

Route::post('time_log_out', time_log_out_Controller::class. '@button_log_out')->name('time_log_out');


Route::get('test',id_Controller::class. '@last_id');

Route::post('task',task_Controller::class. '@getting_task')->name('task');

Route::get('task',SortingTasksController::class.'@showingtasks')->name('task');

Route::get('sortingTasks',SortingTasksController::class.'@sortingTasks')->name('sortingTask',   $firstShownTaskResults, $secondShownTaskResults);

Route::post('checkingTaskState',TaskStateController::class.'@TaskState')->name('checkingTaskState');



//Route::post('save-time', TimeController::class . '@save')->name('save-time'); // Controlleris@metodas

Route::any('adminer', '\Miroc\LaravelAdminer\AdminerController@index');

// Route::resource('time-registration', '\App\Http\Controllers\TimeRegistrationController');
