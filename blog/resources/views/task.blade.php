  <!Doctype>
<html>
<head>
  <title>what is title</title>
</head>
<body>
  <h1>Task</h1>

  <div class="TaskClass">
<form action="{{ url ('task') }}" method="post">
  Task name: <input type="text" name="taskName"  required><br><br>
  Dead line: <input type="date" name="deadLineName"  required><br><br>
  <input type="submit" value="Submit">
  {{csrf_field()}}

  @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
</form>
</div>

<div class="TaskStateClass">

<ul>
  <form action="{{url('checkingTaskState')}}" method="post" >
    {{csrf_field()}}
  @foreach ($tasks as $task)
  <li>
    {{ $task->Task }} state {{ $task->Task_state }} <br>

      <input type="checkbox" name="taskState[{{$task->id}}]" value='{{$task->id}}' {{ $task-> Task_state ? 'checked' : '' }}> Completion<br>
      <input type="hidden" name="allElements[{{$task->id}}]" value='{{$task->id}}'>

      </li>
  @endforeach

<input type="submit" value="Submit">
  </form>

</div>
  </body>
  </html>
