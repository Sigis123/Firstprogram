@extends('template')

@section('body')
<form class="" action="{{ route('time-registration.update', [$timeId]) }}" method="post">
  {{ method_field('PATCH') }}
  {{csrf_field()}}
  <button type="submit">Save</button>
</form>
@endsection
