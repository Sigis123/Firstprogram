<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTaskTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Task', function (Blueprint $table) {
        $table->date('Dead_line')->nullable();
        $table->boolean('Task_state')->default(false);
});
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
  Schema::table('Task', function (Blueprint $table) {
  $table->dropColumn('Dead_line');
  $table->dropColumn('Task_state');
});
    }
}
