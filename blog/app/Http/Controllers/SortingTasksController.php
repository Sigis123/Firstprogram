<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class SortingTasksController extends Controller
{
    public function sortingTasks(){

      $tasks=Task::where('Task_line','0')->get();
      $todayDate=date("Y-m-d");
      foreach($tasks as $task){
      if ($task >= $todayDate)
        $firstShownTaskResults=array_push(array($task));
      else
        $secondShownTaskResults=array_push(array($task));
    }
      // return view('task' , compact('tasks'));
      return redirect()->route('task');
    }
}
