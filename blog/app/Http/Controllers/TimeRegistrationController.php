<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Time;
use Carbon\Carbon;

/**
 *
 */
class TimeRegistrationController extends Controller
{
  public function index()
  {
    $timeId = Time::create();

    return view('time-registration.index', ['timeId' => $timeId->id]);
  }

  public function update(Time $time)
  {
    $time->created_at = Carbon::now();

    return redirect('/time_log_in');
  }

  public function show(Time $time)
  {
    $times = Time::all();

    $sum = 0;
    foreach ($times as $time) {
      $sum += $time->update_at->diffInMinutes($time->created_at);
    }

    return view('time-registration.show', ['time' => $sum]);
  }
}
