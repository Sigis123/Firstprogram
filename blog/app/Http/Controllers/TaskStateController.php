<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Input;


class TaskStateController extends Controller
{
    public function taskState(Request $request)
    {

      $allTasks = Task::whereIn('id', $request->get('allElements'))->get();//daug info kurios nepanaudoju
      $checked = $request->get('taskState'); //cia gaunu skaicius



      foreach ($allTasks as $task) {
        $task->Task_state = isset($checked[$task->id]);
        $task->save();

      }
      return redirect()->route('task');
}
}
