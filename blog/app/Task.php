<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
  protected $table='Task';

  public $timestamps = ['created_at', 'updated_at'];

  public $fillable =['Task'];

}
