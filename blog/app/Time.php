<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
  protected $table = 'time';

  protected $fillable = ['created_at', 'updated_at'];
  //  public $timestamps = ['created_at', 'updated_at'];


  public $timestamps = false;




}
